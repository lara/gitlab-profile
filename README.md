### LARA: Lab for Automated Reasoning and Analysis

 **Helping construct software that does what we expect!**

LARA is a research group led by [Viktor Kunčak](https://people.epfl.ch/viktor.kuncak). We develop precise automated reasoning techniques: tools, algorithms and languages. The goal of these techniques to help construction of verified computer systems.

See, for example:
  * [Stainless](https://gitlab.epfl.ch/lara/stainless) formal software verifier for Scala
  * [Bolts](https://gitlab.epfl.ch/lara/bolts) Stainless case studies
  * [Inox](https://gitlab.epfl.ch/lara/inox) underlying constraint solver
  * [LISA](https://gitlab.epfl.ch/lara/lisa) proof assistant
